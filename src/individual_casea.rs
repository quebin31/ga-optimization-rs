extern crate rand;

use std::fmt;
use std::f64::consts;
use rand::Rng;

use individual;
use individual::Individual;

#[derive(Debug, Clone)]
pub struct IndividualCaseA<T> 
    where T: Clone + fmt::Debug {

    genes: T,
    fitness: f64,
    portion_sum: f64
}

impl Individual<(Self, Self)> for IndividualCaseA<u8> {
    fn random_init() -> Self {
        let mut rng = rand::thread_rng();
        IndividualCaseA {
            genes: rng.gen_range(0, 255),
            fitness: 0.0,
            portion_sum: 0.0
        }    
    }

    fn cross_with(&self, rhs: &Self) -> (Self, Self) {
        let mut child1 = IndividualCaseA { genes: 0u8, fitness: 0.0, portion_sum: 0.0 };
        let mut child2 = IndividualCaseA { ..child1 };

        let mut rng = rand::thread_rng();
        let k = rng.gen_range(1, 8);

        let cross = | mut gen1, mut gen2 | {
            gen1 = gen1 >> (8 - k);
            gen1 = gen1 << (8 - k);

            gen2 = gen2 << k;
            gen2 = gen2 >> k;
        
            gen1 | gen2
        };

        let individual1_genes = self.genes;
        let individual2_genes = rhs.genes;

        child1.genes = cross(individual1_genes, individual2_genes);

        let individual1_genes = rhs.genes;
        let individual2_genes = self.genes;

        child2.genes = cross(individual1_genes, individual2_genes);

        (child1, child2)
    }

    fn calculate_fitness(&mut self) -> f64 {
        let a = unsafe { individual::A };
        let b = unsafe { individual::B }; 
        let x = a + (b - a) * self.genes as f64 / (2.0f64).powi(8);

        let tmp = 10.0 * consts::PI * x;
        self.fitness = x.powi(2) * tmp.sin().powi(2) + 1.0;
        self.fitness
    }

    fn mutate(&mut self, index: usize) {
        let k = 1 << index;
        self.genes = self.genes ^ k;
    }

    fn get_fitness(&self) -> f64 {
        self.fitness
    }
}

impl IndividualCaseA<u8> {
    pub fn get_acc_sum(&self) -> f64 {
        self.portion_sum    
    }

    pub fn set_acc_sum(&mut self, sum: f64) {
        self.portion_sum = sum;
    }

    pub fn transform_gen(&self) -> f64 {
        let a = unsafe { individual::A };
        let b = unsafe { individual::B };
        a + (b - a) * self.genes as f64 / (2.0f64).powi(8)
    }
}
