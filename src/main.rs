extern crate rand;
extern crate criterion_plot as plot;

// traits
mod individual;
mod population;

// custom implementations 
mod individual_casea;
mod population_casea;

mod individual_caseb;
mod population_caseb;

use std::path::Path;

use population::Population;
use individual::Individual;
use population_casea::PopulationCaseA;
use population_caseb::PopulationCaseB; 

use plot::prelude::*;

fn case_a() {
    individual::set_a(0.0);
    individual::set_b(2.0);
    
    let pop_size = 100;
    let no_generations = 500;

    let mut pop: PopulationCaseA<u8> = PopulationCaseA::init_population(pop_size);
    let mut gens = Vec::with_capacity(no_generations);
    let mut mean = Vec::with_capacity(no_generations);
    let mut best = Vec::with_capacity(no_generations);
    let mut acc_sum;

    for gen in 0..no_generations {
        gens.push(gen);
        pop.sort_by_fitness();

        acc_sum = pop.calculate_fit_acc_sum();
        mean.push(acc_sum / pop_size as f64);
        best.push(pop.get_fittest().get_fitness());

        pop.make_roulette();
        pop.cross_population(0.8);
        pop.mutate_population(0.002);
    }
    
    let mut fg = Figure::new();
    fg.configure(Key, | k | {
        k.set(Boxed::Yes)
         .set(Position::Outside(Vertical::Top, Horizontal::Right))
    });

    fg.plot(Lines {
        x: gens.clone(),
        y: mean
    }, | line | {
        line.set(Color::Blue)
            .set(Label("Promedio"))
        
    });

    fg.plot(Lines {
        x: gens.clone(),
        y: best
    }, | line | {
        line.set(Color::Red)
            .set(Label("Mejor"))
    });

    let path = Path::new("./results/casea.svg");
    fg.set(Output(path));
    fg.draw().expect("Failed to plot!");
}

fn case_b() {
    individual::set_a(-200.0);
    individual::set_b( 200.0);
    
    let pop_size = 100;
    let no_generations = 500;
    let mut pop: PopulationCaseB<u8> = PopulationCaseB::init_population(pop_size);
    let mut gens = Vec::with_capacity(no_generations);
    let mut mean = Vec::with_capacity(no_generations);
    let mut best = Vec::with_capacity(no_generations);
    let mut acc_sum;

    for gen in 0..no_generations {
        gens.push(gen);
        pop.sort_by_fitness();

        acc_sum = pop.calculate_fit_acc_sum();
        mean.push(acc_sum / pop_size as f64);
        best.push(pop.get_fittest().get_fitness());
        
        pop.make_roulette();
        pop.cross_population(0.8);
        pop.mutate_population(0.002);
    }

    let mut fg = Figure::new();
    fg.configure(Key, | k | {
        k.set(Boxed::Yes)
         .set(Position::Outside(Vertical::Top, Horizontal::Right))
    });

    fg.plot(Lines {
        x: gens.clone(),
        y: mean
    }, | line | {
        line.set(Color::Blue)
            .set(Label("Promedio"))
        
    });

    fg.plot(Lines {
        x: gens.clone(),
        y: best
    }, | line | {
        line.set(Color::Red)
            .set(Label("Mejor"))
    });

    let path = Path::new("./results/caseb.svg");
    fg.set(Output(path));
    fg.draw().expect("Failed to plot!");

    println!("{:?}", pop.get_fittest());
    println!("{:?}", pop.get_fittest().transform_gen());
}

fn main() {
    case_a();
    case_b();
}
